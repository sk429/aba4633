# aba4633

Data files, model files, and modeling/simulation code related to paper "A triple star system with a misaligned and warped circumstellar disk shaped by disk tearing" (Science magazine, paper aba4633)